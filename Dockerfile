FROM public.ecr.aws/lambda/python:3.8
LABEL Anushka <uyanushka@gmail.com>

# copy artifacts
COPY python ./
COPY requirement.txt .
# install required pip mpdules
RUN pip3 install -r requirement.txt

CMD ["app.lambda_handler"]
