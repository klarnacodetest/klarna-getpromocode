'''
author uyanushka@gmail.com
'''
from asyncio.log import logger
from boto3.dynamodb.conditions import Key


class KeyCondition:

    def getdata_by_keycondition(user_id, campain_id, table, connection):
        ''' Takes in query data from dynamodb
            arguments
            -----------------------
            user_id: id of the user
            campain_id: campain id
            table; dynamodb table name
            connection: dynamodb client
        '''
        try:
            response = connection.get_item(
                TableName=table,
                Key={
                    'user_id': {'S': user_id},
                    'campain_id': {'S': campain_id}
                }
            )

            return response["Item"]

        except KeyError as e:
            logger.info(e)
