'''
author uyanushka@gmail.com
'''
import boto3

from asyncio.log import logger


class Dynamodb:

    def get_connection(aws_region):
        ''' Task to create & returen dynamodb client
            arguments
            -----------------------
            aws_region: aws region dynamodb located on
        '''
        try:
            dynamodb = boto3.client('dynamodb', region_name=aws_region)

            return dynamodb
        except Exception as e:
            logger.info(e)
