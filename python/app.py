'''
author: uyanushka@gmail.com
main python file use to handle the app
'''

import os
import yaml

from asyncio.log import logger
from helpers.keycondition import KeyCondition
from helpers.dynamodbconnector import Dynamodb


def read_config():
    '''
    Task read the config file and return values
    '''
    with open("configs/config.yml") as stream:
        try:
            data = yaml.safe_load(stream)

            return data

        except yaml.YAMLError as e:
            logger.info(e)


def get_promo_code(campain_id, user_id):
    '''
    Task return the promocode
    arguments
    -----------------------
    user_id: id of the user
    campain_id: campain id
    '''
    confgs = read_config()

    region = confgs["region"]
    dynamodb_table = confgs["dynamodb_table"]

    dynamodb_client = Dynamodb.get_connection(region)

    data = KeyCondition.getdata_by_keycondition(
        user_id, campain_id, dynamodb_table, dynamodb_client)

    return data["promo_code"]["S"]


def lambda_handler(event, context):
    '''
    Lambda handler manage the api gateway parameters in headers and body.
    parameters
    ------------
    event['params']['querystring']['campain_id']
    event['params']['querystring']['client_id']
    '''

    campain_id = event['params']['querystring']['campain_id']
    client_id = event['params']['querystring']['client_id']
    data = get_promo_code(campain_id, client_id)

    return {
        "promocode": data
    }
