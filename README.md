# Klarna-getpromocode

this is use for get promo code from the dynamodb

## Deploy Klarna-getpromocode

* Create an ECR klarna-getpromocode.
* Clone the repo
* Go inside the project and build the docker image.

    ```
    cd klarna-getpromocode
    docker build -t  klarna-getpromocode .
    docker tag klarna-getpromocode:latest <-aws account -d->.dkr.ecr.us-east-1.amazonaws.com/klarna-getpromocode:latest
    docker push <-aws account -d->..dkr.ecr.us-east-1.amazonaws.com/klarna-getpromocode:latest
    ```
* Create a Dynamodb table called klarna-getpromocode.
    - Create an IAM for lambda function that should have "dynamodb:GetItem" access. You can find the IAM policy from permission dir in repo.
    - Navigate to the lambda section in aws console.
    - Click the button create function in the top right corner.
    - Now choose the container image option and provide the name for the lambda function in this case name is klarna-getpromocode.
    - Choose the upload docker image in the above step.
    - Choose the IAM role which was created.
    - Then create the lambda function.

* Create an API gateway.
    - Navigate to the API GateWay section.
    - Then click on the Create API button.
    - Then select REST API.
    - Provide API name I’m using klarna-getpromocode-api
    - Once done select Action then Create Method and select GET.
    - Select Integration type as Lambda Function.
    - Choose the region.

